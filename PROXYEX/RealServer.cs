﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PROXYEX
{
    class RealServer : Interface1
    {
        //se implementa el servidor real
    

        private int port;
        private String host;


        public RealServer(int port, String host)
        {
            this.port = port;
            this.host = host;
            /Console.WriteLine("Servidor iniciado...");

        }

        internal Interface1 Interface1
        {
            get => default;
            set
            {
            }
        }

        internal Interface1 Interface11
        {
            get => default;
            set
            {
            }
        }

        public void Descarga(String url)
        {
            Console.WriteLine("Descargando " + host + ":" + port + "/" + url);
        }

    }
}

