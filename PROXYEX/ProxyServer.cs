﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace PROXYEX
{
    public class ProxyServer : Interface1  //Se implementa el proxy que incluyo el control de acceso restringido
    {
        RealServer srv;
        public int port;
        public string host;

        public ProxyServer(int port, string host)
        {
            this.port = port;
            this.host = host;
            srv = null;
            Console.WriteLine("Proxy iniciado...");
        }

        internal Interface1 Interface1
        {
            get => default;
            set
            {
            }
        }

        internal RealServer RealServer
        {
            get => default;
            set
            {
            }
        }

        internal Interface1 Interface11
        {
            get => default;
            set
            {
            }
        }

        internal RealServer RealServer1
        {
            get => default;
            set
            {
            }
        }

        public void Descarga(String url)
        {

            if (IsRestricted(url))
            {
                if (srv == null)
                {
                    srv = new RealServer(port, host);
                }
                srv.Descarga(url);
            }
            else
            {
                Console.WriteLine("Actualmente se encuentra en un área que no permite la descarga del fichero.");
            }
        }

        public Boolean IsRestricted(String fichero)
        {
            return !fichero.Equals("/descarga/prohibida.avi");
        }
    }
}