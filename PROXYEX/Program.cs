﻿using System;

namespace PROXYEX
{
   public class Program
    {
        internal Interface1 Interface1 // interaccion con el cliente 
        {
            get => default;
            set
            {
            }
        }

        public static void Main(string[] args)
        {

            // Creamos el proxy a la página de descargas

            Interface1 srv = new ProxyServer(20, " https://programacion.net ");


            //Descargamos un archivo permitido

            srv.Descarga("/descarga/permitida.avi");

            //// En este punto será donde se cree el objeto RealServer

            //// Vamos a probar ahora con una descarga restringida

            srv.Descarga("/descarga/prohibida.avi");

        }
    }
}
